import { ROUTES_COMPONENTS } from "app/core/const/routes/routes.components";
import AuthContextProvider from "app/data/contexts/AuthContext";
import CarContextProvider from "app/data/contexts/CarContext";
import { Suspense } from "react";
import { HelmetProvider } from "react-helmet-async";
import { BrowserRouter, Route, Routes } from "react-router-dom";

import "./sass/App.scss";

function App() {
  return (
    <BrowserRouter>
      <HelmetProvider>
        <AuthContextProvider>
          <CarContextProvider>
            <Suspense fallback={null}>
              <Routes>
                <Route
                  path={ROUTES_COMPONENTS.HOME.PATH}
                  element={<ROUTES_COMPONENTS.HOME.COMPONENT />}
                />
                <Route
                  path={ROUTES_COMPONENTS.CARS.PATH}
                  element={<ROUTES_COMPONENTS.CARS.COMPONENT />}
                />
                <Route
                  path={ROUTES_COMPONENTS.CARS_DETAIL.PATH}
                  element={<ROUTES_COMPONENTS.CARS_DETAIL.COMPONENT />}
                />
                <Route
                  path={ROUTES_COMPONENTS.QUOTER.PATH}
                  element={<ROUTES_COMPONENTS.QUOTER.COMPONENT />}
                />
                <Route
                  path={ROUTES_COMPONENTS.SIGNUP.PATH}
                  element={<ROUTES_COMPONENTS.SIGNUP.COMPONENT />}
                />
                <Route
                  path={ROUTES_COMPONENTS.FORGOT_PASSWORD.PATH}
                  element={<ROUTES_COMPONENTS.FORGOT_PASSWORD.COMPONENT />}
                />
                <Route
                  path={ROUTES_COMPONENTS.RESET_PASSWORD.PATH}
                  element={<ROUTES_COMPONENTS.RESET_PASSWORD.COMPONENT />}
                />
                <Route
                  path={ROUTES_COMPONENTS.NOT_FOUND.PATH}
                  element={<ROUTES_COMPONENTS.NOT_FOUND.COMPONENT />}
                />
              </Routes>
            </Suspense>
          </CarContextProvider>
        </AuthContextProvider>
      </HelmetProvider>
    </BrowserRouter>
  );
}

export default App;
