import SimpleSpinnerComponent from 'app/shared/components/spinners/SimpleSpinnerComponent/SimpleSpinnerComponent';
import { useFormik } from 'formik';
import { useState } from 'react';
import './form-quoter-component.scss';

const FormQuoterComponent = ({ close, clear }) => {

    const [sending, setSending] = useState(false);

    const validate = values => {
        const errors = {};
        if (!values.name) {
            errors.name = 'Required';
        } else if (values.name.length > 15) {
            errors.name = 'Must be 15 characters or less';
        }

        if (!values.lastName) {
            errors.lastName = 'Required';
        } else if (values.lastName.length > 20) {
            errors.lastName = 'Must be 20 characters or less';
        }

        if (!values.email) {
            errors.email = 'Required';
        } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = 'Invalid email address';
        }

        return errors;
    };

    const formik = useFormik({
        initialValues: {
            name: '',
            lastName: '',
            email: '',
        },
        validate,
        onSubmit: values => {
            setSending(true);
            console.log(values);
            setTimeout(() => {
                setSending(false);
                clear();
                close();
            }, 2000);
        },
    });

    return (
        <div className='app__form'>
            <form onSubmit={formik.handleSubmit}>
                <div className="col-12 mb-3">
                    <label className="form-label" htmlFor="name">First Name</label>
                    <input
                        id="name"
                        name="name"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.name}
                        className={`form-control ${formik.errors.name ? 'error' : ''}`}
                        placeholder='Name'
                    />
                    {formik.errors.name ? <p className='error'>{formik.errors.name}</p> : null}
                </div>

                <div className="col-12 mb-3">
                    <label className="form-label" htmlFor="lastName">Last Name</label>
                    <input
                        id="lastName"
                        name="lastName"
                        type="text"
                        onChange={formik.handleChange}
                        value={formik.values.lastName}
                        className={`form-control ${formik.errors.lastName ? 'error' : ''}`}
                        placeholder='Last Name'
                    />
                    {formik.errors.lastName ? <p className='error'>{formik.errors.lastName}</p> : null}
                </div>

                <div className="col-12 mb-3">
                    <label className="form-label" htmlFor="email">Email</label>
                    <input
                        id="email"
                        name="email"
                        type="email"
                        onChange={formik.handleChange}
                        value={formik.values.email}
                        className={`form-control ${formik.errors.email ? 'error' : ''}`}
                        placeholder='Last Name'
                    />
                    {formik.errors.email ? <p className='error'>{formik.errors.email}</p> : null}
                </div>

                <div className="col-12">
                    {sending ? <SimpleSpinnerComponent /> : <button className='btn btn-more light-blue' type="submit">Submit</button>}

                </div>
            </form>
        </div>
    );
};

export default FormQuoterComponent;