import { Helmet } from "react-helmet-async";
import { useParams } from "react-router-dom";
//
import "./quoter-page.scss";
import { calculateFinalPrice, numberFormat } from "app/core/utils/utils";
import useQuoter from "app/data/hooks/useQuoter";
import Layout from "app/layout/default/Layout";
import TitleComponent from "app/shared/components/title/TitleComponent";
import SimpleSpinnerComponent from "app/shared/components/spinners/SimpleSpinnerComponent/SimpleSpinnerComponent";
import OptionsComponent from "app/shared/components/options/OptionsComponent";
import OptionItemComponent from "app/shared/components/options/OptionItemComponent";
import ModalService from "app/shared/components/modal-service/ModalService";
import useQuoterReducer from "app/modules/quoter/reducer/useQuoterReducer";
import FormQuoterComponent from "../components/FormQuoterComponent";
import ModalRoot from "app/shared/components/modal-service/ModalRoot";

const QuoterPage = () => {
  let params = useParams();
  const { slug } = params;
  const { car, options, isLoading, error } = useQuoter({ slug });
  const { state, selectOption, resetOptions } = useQuoterReducer({
    options,
    price: car?.price,
    discount: car?.discount,
  });
  //
  const isLoadingClass = isLoading ? "is-loading" : "";
  //
  const handleModal = () => {
    ModalService.open({
      component: FormQuoterComponent,
      title: "Quoter",
      props: {
        clear: () => {
          resetOptions(options);
        }
      }
    });
  };
  //
  return (
    <>
      <Helmet>
        <title>Customize Your Car</title>
      </Helmet>
      <Layout className="car-detail-page" back={true} hasError={error}>
        <section
          className={`app__content_body app__quoter-detail ${isLoadingClass}`}
        >
          {!car || isLoading ? (
            <SimpleSpinnerComponent />
          ) : (
            <>
              <article className="app__car-detail_content">
                <div className="app__car-detail_body">
                  <TitleComponent
                    leyend="Choose Your Parts"
                    subTitle=" Your car"
                    title="Customize"
                  />

                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-12">
                        <p>Please choose the pieces of your preference</p>
                        <p>
                          <small>(*) Required</small>
                        </p>
                      </div>
                    </div>
                  </div>

                  <div className="app__options_box">
                    {state.options.map(
                      ({ id, name, required, items }, option_index) => {
                        const parent = id;
                        return (
                          <OptionsComponent
                            key={parent + option_index}
                            name={name}
                            required={required}
                          >
                            {items.map(
                              (
                                { id, name, isSelected, image, price },
                                item_index
                              ) => (
                                <OptionItemComponent
                                  key={id + item_index}
                                  image={image}
                                  name={name}
                                  isSelected={isSelected}
                                  price={price}
                                  onClick={() => {
                                    selectOption({ parent, id });
                                  }}
                                />
                              )
                            )}
                          </OptionsComponent>
                        );
                      }
                    )}
                  </div>
                  <div className="app__car-detail_fixed-btn-box">
                    <div className="app__car-quoter_price mobile">
                      <p className="price_value">
                        Total:{" "}
                        {numberFormat(
                          calculateFinalPrice(car.price, car.discount) +
                          state.total
                        )}
                      </p>
                    </div>
                    <button className="app__car-detail_fixed-btn" disabled={!state.isValid} onClick={handleModal}>
                      Send Quoter
                    </button>
                  </div>
                </div>

                <div className="app__car-detail_image-box">
                  <div className="app__car-detail_image-box_fixed">
                    <div className="app__car-quoter_price">
                      <p className="price_value">
                        Total:{" "}
                        {numberFormat(
                          calculateFinalPrice(car.price, car.discount) +
                          state.total
                        )}
                      </p>
                    </div>
                    <div className="app__car-detail_image">
                      <img src={car.image} alt="car" />
                    </div>
                  </div>
                </div>
              </article>
            </>
          )}
          <ModalRoot />
        </section>
      </Layout>

    </>
  );
};

export default QuoterPage;
