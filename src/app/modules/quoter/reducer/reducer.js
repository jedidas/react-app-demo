export default function quoterReducer(state, action) {
  switch (action.type) {
    case QUOTER_REDUCER_ACTIONS.UPDATE_OPTIONS: {
      return { ...state, options: action.payload };
    }
    case QUOTER_REDUCER_ACTIONS.SELECT_OPTION: {
      return { ...state, options: action.payload };
    }
    case QUOTER_REDUCER_ACTIONS.UPDATE_TOTAL: {
      return { ...state, total: action.payload };
    }
    case QUOTER_REDUCER_ACTIONS.UPDATE_IS_VALID: {
      return { ...state, isValid: action.payload };
    }

    default:
      return state;
  }
}

export const QUOTER_REDUCER_ACTIONS = {
  UPDATE_OPTIONS: "UPDATE_OPTIONS",
  SELECT_OPTION: "SELECT_OPTION",
  UPDATE_TOTAL: "UPDATE_TOTAL",
  UPDATE_IS_VALID: "UPDATE_IS_VALID",
};
