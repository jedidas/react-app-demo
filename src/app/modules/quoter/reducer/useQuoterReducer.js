import { deepCopyFunction, getSelectedItemsCount } from "app/core/utils/utils";
import { useEffect, useReducer } from "react";
import quoterReducer, { QUOTER_REDUCER_ACTIONS } from "./reducer";

export default function useQuoterReducer({ total = 0, options }) {
  const [state, dispatch] = useReducer(quoterReducer, {
    options,
    total,
    isValid: false,
  });

  useEffect(() => {
    dispatch({
      type: QUOTER_REDUCER_ACTIONS.UPDATE_OPTIONS,
      payload: options,
    });
  }, [options]);

  const resetOptions = () => {
    state.options.map((option) =>
      option.items.map((item) => {
        item.isSelected = false;
        return item;
      })
    );
    dispatch({
      type: QUOTER_REDUCER_ACTIONS.UPDATE_OPTIONS,
      payload: state.options,
    });
    validateOptions();
  };

  const selectOption = ({ parent, id }) => {
    const foundedOption = state.options.filter((option) =>
      option.items.some((item) => item.id === id)
    )[0];
    const foundedItem = foundedOption.items.filter((item) => {
      return item.id === id;
    })[0];

    if (foundedItem.isSelected) {
      foundedItem.isSelected = false;
    } else if (
      getSelectedItemsCount(foundedOption.items, "isSelected", true) <
      foundedOption.max_count
    ) {
      foundedItem.isSelected = !foundedItem.isSelected;
    }
    //
    updateTotal();
    validateOptions();
    //
    dispatch({
      type: QUOTER_REDUCER_ACTIONS.SELECT_OPTION,
      payload: deepCopyFunction(state.options),
    });
  };

  const validateOptions = () => {
    const newState = state.options.reduce((acc, option) => {
      if (option.required) {
        return (
          getSelectedItemsCount(option.items, "isSelected", true) ===
            option.max_count && acc
        );
      }
      return acc;
    }, true);
    dispatch({
      type: QUOTER_REDUCER_ACTIONS.UPDATE_IS_VALID,
      payload: newState,
    });
  };

  const updateTotal = () => {
    const result = state.options.reduce((acc, item) => {
      return (
        acc +
        item.items.reduce((acc2, item2) => {
          if (item2.isSelected) {
            return acc2 + item2.price;
          }
          return acc2;
        }, 0)
      );
    }, 0);
    dispatch({
      type: QUOTER_REDUCER_ACTIONS.UPDATE_TOTAL,
      payload: result,
    });
  };

  return {
    state,
    selectOption,
    updateTotal,
    resetOptions,
  };
}
