import "./cars-page.scss";
//
import { INTERNAL_ROUTES } from "app/core/const/routes/internal.routes";
//
import TitleComponent from "app/shared/components/title/TitleComponent";
import CarroucelComponent from "app/shared/components/carroucel/CarroucelComponent";

//
import CardCarComponent from "app/modules/cars/components/card-car/CardCarComponent";
import CarInfoComponent from "app/modules/cars/components/car-info/CarInfoComponent";
import CarInfoItemComponent from "app/modules/cars/components/car-info-item/CarInfoItemComponent";
import { Helmet } from "react-helmet-async";
import Layout from "app/layout/default/Layout";
import useCar from "app/data/hooks/useCar";
import SimpleSpinnerComponent from "app/shared/components/spinners/SimpleSpinnerComponent/SimpleSpinnerComponent";
import { useRef } from "react";

const CarsPage = () => {
  const { cars, isLoading, currentCar, updateCurrentCar, error } = useCar();
  const slideRef = useRef();
  const isLoadingClass = isLoading ? "is-loading" : "";

  const handleClick = (index) => {
    slideRef.current.slickGoTo(index);
  };

  return (
    <>
      <Helmet>
        <title>New Car List</title>
      </Helmet>
      <Layout className="car-page" hasError={error}>
        <section
          className={`app__content_body app__cars-page ${isLoadingClass}`}
        >
          {isLoading ? (
            <SimpleSpinnerComponent />
          ) : (
            <>
              <TitleComponent
                leyend="The Best Veicles"
                subTitle="List"
                title="New Car"
              />
              <CarroucelComponent
                beforeChange={updateCurrentCar}
                slideRef={slideRef}
              >
                {cars.map(
                  (
                    { id, slug, name, price, discount, code, image, color },
                    index
                  ) => (
                    <CardCarComponent
                      price={price}
                      code={code}
                      discount={discount}
                      id={id}
                      image={image}
                      key={id}
                      name={name}
                      onClick={() => handleClick(index)}
                      path={INTERNAL_ROUTES.CARS_DETAIL + slug}
                      slug={slug}
                    />
                  )
                )}
              </CarroucelComponent>

              <CarInfoComponent visible={currentCar.visible}>
                <CarInfoItemComponent
                  title={"Passenger"}
                  icon="icon-passenger"
                  value={currentCar.data.passenger}
                />
                <CarInfoItemComponent
                  title={"Transmission"}
                  icon="icon-transmission"
                  value={currentCar.data.transmission}
                />
                <CarInfoItemComponent
                  title={"HP"}
                  icon="icon-engine"
                  value={currentCar.data.hp}
                />
                <CarInfoItemComponent
                  title={"Top Speed"}
                  icon="icon-speed"
                  value={currentCar.data.speed}
                />
              </CarInfoComponent>
            </>
          )}
        </section>
      </Layout>
    </>
  );
};

export default CarsPage;
