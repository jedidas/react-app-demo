import { calculateFinalPrice, numberFormat } from "app/core/utils/utils";
import { Link } from "react-router-dom";

import "./card-car-component.scss";

const CardCarComponent = ({
  discount,
  id,
  image,
  name,
  onClick,
  price,
  path,
}) => {
  return (
    <div className="card-car-component" data-id={`car-item-${id}`}>
      <div className="card-car-component__body">
        <div className="card-car-component__body_" onClick={onClick}>
          <h3 className="card-car-component__title">{name}</h3>
          <p className="card-car-component__price">
            ${numberFormat(calculateFinalPrice(price, discount))}
          </p>
          <div className="card-car-component__img">
            <img src={image} alt={name} />
          </div>
        </div>
        <div className="card-car-component__more">
          <Link className="btn btn-more light-blue" to={path}>
            See Detail
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CardCarComponent;
