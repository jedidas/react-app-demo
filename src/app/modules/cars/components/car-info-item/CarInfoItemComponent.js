import React from "react";

import "./car-info-item-component.scss";

const CarInfoItemComponent = ({
  children = null,
  icon = "icon-code",
  title = "",
  value = "",
}) => {
  if (children) {
    return (
      <div className="app__car-info-component_item">
        {children}
        <p>
          <span>{title}</span>
        </p>
      </div>
    );
  }

  return (
    <div className="app__car-info-component_item">
      <span className={icon}></span>
      <p>
        {value}
        <span>{title}</span>
      </p>
    </div>
  );
};

export default React.memo(CarInfoItemComponent);
