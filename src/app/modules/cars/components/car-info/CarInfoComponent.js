import "./car-info-component.scss";

const CarInfoComponent = ({ children, visible = false }) => {
  return (
    <div
      className={
        visible ? "app__car-info-component show" : "app__car-info-component"
      }
    >
      <div className="app__car-info-component_item-box">{children}</div>
    </div>
  );
};

export default CarInfoComponent;
