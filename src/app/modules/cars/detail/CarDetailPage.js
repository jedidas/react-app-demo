import TitleComponent from "app/shared/components/title/TitleComponent";
import CarInfoItemComponent from "app/modules/cars/components/car-info-item/CarInfoItemComponent";
//
import "./car-detail-page.scss";
import { Helmet } from "react-helmet-async";
import Layout from "app/layout/default/Layout";
import useDetail from "app/data/hooks/useDetail";
import SimpleSpinnerComponent from "app/shared/components/spinners/SimpleSpinnerComponent/SimpleSpinnerComponent";
import { Link, useParams } from "react-router-dom";
import React from "react";
import { numberFormat } from "app/core/utils/utils";
import { INTERNAL_ROUTES } from "app/core/const/routes/internal.routes";

const CarDetailPage = () => {
  let params = useParams();
  const { slug } = params;
  const { car, isLoading, error } = useDetail({ slug });
  const isLoadingClass = isLoading ? "is-loading" : "";
  return (
    <>
      <Helmet>
        <title>{error ? "Error" : "Detail " + car?.name}</title>
      </Helmet>
      <Layout className="car-detail-page" hasError={error}>
        <section
          className={`app__content_body app__car-detail ${isLoadingClass}`}
        >
          {!car || isLoading ? (
            <SimpleSpinnerComponent />
          ) : (
            <article className="app__car-detail_content">
              <div className="app__car-detail_body">
                <TitleComponent
                  className="text-left"
                  leyend="Information"
                  subTitle="Atque saepe numquam"
                  title={car.name}
                />
                <div className="app__car-detail_price">
                  <p className="price_value">
                    Price<strong>${numberFormat(car.price)}</strong>
                  </p>
                  <p className="price_discount">
                    Discount<strong>{car.discount}%</strong>
                  </p>
                </div>
                <div className="app__car-detail_info">
                  <h4 className="subtitle">Features</h4>
                  <div className="app__car-detail_info-features">
                    <CarInfoItemComponent
                      title={"Passenger"}
                      icon="icon-passenger"
                      value={car.passenger}
                    />
                    <CarInfoItemComponent
                      title={"Transmission"}
                      icon="icon-transmission"
                      value={car.transmission}
                    />
                    <CarInfoItemComponent
                      title={"HP"}
                      icon="icon-engine"
                      value={car.hp}
                    />
                    <CarInfoItemComponent
                      title={"Top Speed"}
                      icon="icon-speed"
                      value={car.speed}
                    />
                    <CarInfoItemComponent
                      title={"Code"}
                      icon="icon-code"
                      value={car.code}
                    />
                  </div>

                  <article className="app__car-detail_info-description">
                    <h4 className="subtitle">Description</h4>
                    <p>{car.description}</p>
                  </article>
                </div>

                <div className="app__car-detail_fixed-btn-box">
                  <Link
                    to={INTERNAL_ROUTES.QUOTER + slug}
                    className="app__car-detail_fixed-btn"
                  >
                    diy your car
                  </Link>
                </div>
              </div>

              <div className="app__car-detail_image-box">
                <div className="app__car-detail_image-box_fixed">
                  <div className="app__car-detail_image">
                    <img src={car.image} alt={car.name} />
                  </div>
                </div>
              </div>
            </article>
          )}
        </section>
      </Layout>
    </>
  );
};

export default CarDetailPage;
