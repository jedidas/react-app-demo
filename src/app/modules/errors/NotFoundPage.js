import Layout from "app/layout/default/Layout";
import React from "react";
import { Helmet } from "react-helmet-async";

const NotFoundPage = () => {
  return (
    <>
      <Helmet>
        <title>Page Not Found</title>
      </Helmet>
      <Layout className="car-page" hasError={"404,Page Not Found"} />
    </>
  );
};

export default React.memo(NotFoundPage);
