import Layout from "app/layout/default/Layout";
import TitleComponent from "app/shared/components/title/TitleComponent";
import { Helmet } from "react-helmet-async";
import "./home-page.scss";

const HomePage = () => {
  return (
    <>
      <Helmet>
        <title>Welcome</title>
      </Helmet>
      <Layout className="home-page" back={false}>
        <section className="app__content_body app__home-page">
          <TitleComponent
            leyend="The Best App Ever"
            subTitle=" to Super App"
            title="Welcome"
          />
        </section>
      </Layout>
    </>
  );
};

export default HomePage;
