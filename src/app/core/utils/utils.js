export const numberFormat = Intl.NumberFormat("en-US").format;

export const generateArrayWithObject = (object, count) => {
  return Array.from({ length: count }, () => Object.assign({}, object));
};

export const getRandomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const calculateFinalPrice = (value, discount) => {
  return value - (value * discount) / 100;
};

export const getErrorRequest = (error) => {
  if (Boolean(error)) {
    let values = error.split(",");
    if (values.length > 1) {
      return {
        errorMessage: values[1],
        errorCode: values[0],
      };
    }
  }
  return {
    errorMessage: error,
    errorCode: "Error",
  };
};

//
export const hasItem = (array, key, value) => {
  return array.some((item) => item[key] === value);
};
//
export const getSelectedItemsCount = (items, key, value) => {
  return items.reduce((count, item) => {
    return count + (item[key] ? value : 0);
  }, 0);
};

export const findIElementFromArrayAndUpdate = ({
  array,
  key,
  value,
  update,
}) => {
  return array.map((item) => {
    if (item[key] === value) {
      item = update(item);
    }
    return { ...item };
  });
};

export const deepCopyFunction = (inObject) => {
  let outObject, value, key;

  if (typeof inObject !== "object" || inObject === null) {
    return inObject;
  }
  outObject = Array.isArray(inObject) ? [] : {};

  for (key in inObject) {
    value = inObject[key];
    outObject[key] = deepCopyFunction(value);
  }
  return outObject;
};
