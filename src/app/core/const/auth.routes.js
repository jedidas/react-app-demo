const AUTH_ROUTES = {
  LOGIN: `${process.env.REACT_APP_CARS_API_BASE}/login`,
  LOGOUT: `${process.env.REACT_APP_CARS_API_BASE}/logout`,
  SING_UP: `${process.env.REACT_APP_CARS_API_BASE}/sign-up`,
  FORGOT_PASSWORD: `${process.env.REACT_APP_CARS_API_BASE}/forgot-password`,
  RESET_PASSWORD: `${process.env.REACT_APP_CARS_API_BASE}/reset-password`,
};
export default AUTH_ROUTES;
