import { INTERNAL_PATHS, INTERNAL_ROUTES } from "./internal.routes";

const PUBLIC_MENU_ROUTES = [
  {
    title: "Home",
    path: INTERNAL_ROUTES.HOME,
    icon: 'icon-home',
  },
  {
    title: "Cars",
    path: INTERNAL_ROUTES.CARS,
    icon: 'icon-car',
  },
  {
    title: "Error Page",
    path: INTERNAL_PATHS.QUOTER_DEFAULT,
    icon: 'icon-options',
  },
];
export default PUBLIC_MENU_ROUTES;
