import { INTERNAL_ROUTES } from "./internal.routes";

import HomePage from "app/modules/home/index/HomePage";
//
import CarsPage from "app/modules/cars/index/CarsPage";
import CarDetailPage from "app/modules/cars/detail/CarDetailPage";
//
import QuoterPage from "app/modules/quoter/index/QuoterPage";
//
import SignupPage from "app/modules/auth/signup/SignupPage";
import ForgotPasswordPage from "app/modules/auth/forgot-password/ForgotPasswordPage";
import ResetPasswordPage from "app/modules/auth/reset-password/ResetPasswordPage";
//
import NotFoundPage from "app/modules/errors/NotFoundPage";

export const ROUTES_COMPONENTS = {
  HOME: {
    PATH: INTERNAL_ROUTES.HOME,
    COMPONENT: HomePage,
  },
  CARS: {
    PATH: INTERNAL_ROUTES.CARS,
    COMPONENT: CarsPage,
  },
  CARS_DETAIL: {
    PATH: `${INTERNAL_ROUTES.CARS_DETAIL}:slug`,
    COMPONENT: CarDetailPage,
  },
  QUOTER: {
    PATH: `${INTERNAL_ROUTES.QUOTER}:slug`,
    COMPONENT: QuoterPage,
  },
  SIGNUP: {
    PATH: INTERNAL_ROUTES.SIGNUP,
    COMPONENT: SignupPage,
  },
  FORGOT_PASSWORD: {
    PATH: INTERNAL_ROUTES.FORGOT_PASSWORD,
    COMPONENT: ForgotPasswordPage,
  },
  RESET_PASSWORD: {
    PATH: `${INTERNAL_ROUTES.RESET_PASSWORD}:token`,
    COMPONENT: ResetPasswordPage,
  },
  NOT_FOUND: {
    PATH: INTERNAL_ROUTES.NOT_FOUND,
    COMPONENT: NotFoundPage,
  },
};
