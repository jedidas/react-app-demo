const API_ROUTES = {
  PRODUCTS: {
    IMAGE: process.env.REACT_APP_CARS_API_image,
    DEFAULT: `${process.env.REACT_APP_CARS_API_BASE}/products`,
    DETAIL: `${process.env.REACT_APP_CARS_API_BASE}/products`,
    DETAIL_SLUG: `${process.env.REACT_APP_CARS_API_BASE}/products/REPACE_ME/detail`,
    DETAIL_FULL_SLUG: `${process.env.REACT_APP_CARS_API_BASE}/products/REPACE_ME/detail?all=true`,
  }
};

export default API_ROUTES;
