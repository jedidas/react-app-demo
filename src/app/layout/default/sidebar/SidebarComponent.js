import { useLocation } from "react-router-dom";
//
import "./sidebar-component.scss";
import logotype from "assets/logotype/logo.svg";
import SidebarMenuItemComponent from "app/layout/default/components/SidebarMenuItemComponent";
import PUBLIC_MENU_ROUTES from "app/core/const/routes/public.menu.routes";
import React from "react";
import ModalService from "app/shared/components/modal-service/ModalService";
import LoginComponent from "app/shared/components/auth/LoginComponent";
import ModalRoot from "app/shared/components/modal-service/ModalRoot";
import useAuth from "app/data/hooks/useAuth";

const SidebarComponent = ({ active = false }) => {
  const location = useLocation();

  const isActive = (path, className = "") => {
    const { pathname } = location;
    return pathname === path ? `active` : className;
  };

  const { isLoggedIn, logout, user } = useAuth();

  const handleLogin = () => {
    ModalService.open({
      component: LoginComponent,
      title: "Login",
    });
  };

  return (
    <aside className={active ? "app__sidebar active" : "app__sidebar"}>
      <div className="app__sidebar_logotype">
        <img src={logotype} alt="logotype" />
      </div>
      <nav>
        <ul className="app__sidebar_menu">
          {PUBLIC_MENU_ROUTES.map(({ title, path, icon }, index) => (
            <SidebarMenuItemComponent
              key={path + index}
              title={title}
              path={path}
              icon={icon}
              className={isActive(path)}
            />
          ))}

          <li className="app__sidebar_menu-item">
            {isLoggedIn ? (
              <button onClick={logout}>Sign Out</button>
            ) : (
              <button onClick={handleLogin}>Sign In</button>
            )}
          </li>
        </ul>
        {isLoggedIn ? (
          <div className="app__sidebar_user-info">
            <div className="app__sidebar_user-info_avatar">
              <img src={user.avatar} alt={user.name} />
            </div>
            <p>{user.email}</p>
          </div>
        ) : null}
      </nav>

      <ModalRoot />
    </aside>
  );
};

export default React.memo(SidebarComponent);
