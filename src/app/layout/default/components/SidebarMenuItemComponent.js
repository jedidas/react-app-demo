import React from "react";
import { Link } from "react-router-dom";

const SidebarMenuItemComponent = ({ className, icon = "", path, title }) => {
  return (
    <li className="app__sidebar_menu-item">
      <Link className={className} to={path}>
        <span className={"icon " + icon}></span>
        {title}
      </Link>
    </li>
  );
};

export default React.memo(SidebarMenuItemComponent);
