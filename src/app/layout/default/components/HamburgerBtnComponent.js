import "./hamburger-btn-component.scss";

const HamburgerBtnComponent = ({
  active = false,
  hangleClick = () => {
    /**/
  },
}) => {
  const activeClass = active
    ? "app__header_hamburger-btn is-active"
    : " app__header_hamburger-btn";
  return (
    <button className={activeClass} onClick={hangleClick}>
      <span className="hamburger hamburger--squeeze">
        <span className="hamburger-box">
          <span className="hamburger-inner"></span>
        </span>
      </span>
    </button>
  );
};

export default HamburgerBtnComponent;
