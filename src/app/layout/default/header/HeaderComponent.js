import React from "react";
import "./header-component.scss";

const HeaderComponent = ({ back = false, children }) => {
  const backClass = back ? "has-back" : "";
  return <header className={`app__header ${backClass}`}>{children}</header>;
};

export default React.memo(HeaderComponent);
