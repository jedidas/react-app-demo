import "./layout.scss";
import HeaderComponent from "./header/HeaderComponent";
import SidebarComponent from "./sidebar/SidebarComponent";
import HamburgerBtnComponent from "./components/HamburgerBtnComponent";
import React, { useState } from "react";
import { getErrorRequest } from "app/core/utils/utils";
import { useNavigate } from "react-router-dom";

const Layout = ({ back=true, className = "", children, hasError = null }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  const navigate = useNavigate();
  //
  const hasErrorClass = hasError ? "has-error" : "";
  let ErrorMessageComponent = '';
  const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  if(hasError) {
    ErrorMessageComponent =React.lazy(() => import("app/shared/components/error/ErrorMessageComponent"));
  }

  const { errorMessage, errorCode } = getErrorRequest(hasError);

  return (
    <>
      <SidebarComponent active={isSidebarOpen} />
      <div className={`app__content ${className} ${hasErrorClass}`}>
        <HeaderComponent back={back}>
          {back &&  <button className="btn-back" onClick={() => navigate(-1)}><span className="icon-back"></span></button>}
          <HamburgerBtnComponent
            active={isSidebarOpen}
            hangleClick={toggleSidebar}
          />
        </HeaderComponent>
        {hasError ? (
          <ErrorMessageComponent message={errorMessage} code={errorCode} />
        ) : (
          children
        )}
      </div>
    </>
  );
};

export default Layout;
