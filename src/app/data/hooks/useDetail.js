import API_ROUTES from "app/core/const/api.routes";
import { useContext, useEffect, useState } from "react";
import { CarContext } from "../contexts/CarContext";
import carService from "../services/apis/CarService";

export default function useDetail({ slug }) {
  const { findBySlug } = useContext(CarContext);
  const [state, setState] = useState({ isLoading: false, error: null });
  const [car, setCar] = useState(findBySlug(slug));

  useEffect(() => {
    if (!car) {
      setState({ isLoading: true, error: null });
      carService
        .getDetails(slug)
        .then(({ data }) => {
          data = { ...data, image: API_ROUTES.PRODUCTS.IMAGE + data.image };
          setCar(data);
          setState({ isLoading: false, error: null });
        })
        .catch((errorRes) => {
          setState({ isLoading: false, error: errorRes.message });
        });
    }
  }, [car, slug]);

  return {
    car,
    isLoading: state.isLoading,
    error: state.error,
  };
}
