import { useContext, useEffect, useState } from "react";
//
import { CarContext } from "app/data/contexts/CarContext";
import carService from "app/data/services/apis/CarService";
import API_ROUTES from "app/core/const/api.routes";

export default function useCar() {
  const { cars, setCar } = useContext(CarContext);
  const [state, setState] = useState({ isLoading: false, error: null });
  const [currentCar, setCurrentCar] = useState({
    visible: false,
    data: {},
  });

  useEffect(() => {
    if (cars.data.length > 0) {
      setCurrentCar({ visible: true, data: cars.data[0] });
      return;
    }

    setState({ isLoading: true, error: null });
    carService
      .getAll()
      .then(({ data, from, last_page, total }) => {
        const newData = data.map((item) => {
          item.image = API_ROUTES.PRODUCTS.IMAGE + item.image;
          return item;
        });
        setCar({ data: newData, from, last_page, total });
        setCurrentCar({ visible: true, data: newData[0] });
        setState({ isLoading: false, error: null });
      })
      .catch((errorRes) => {
        setState({ isLoading: false, error: errorRes.message });
      });
  }, [cars.data, setCar]);

  const updateCurrentCar = (index) => {
    if (cars.data[index].id !== currentCar.data.id) {
      setCurrentCar((prevState) => ({ ...prevState, visible: false }));
      const temp = setTimeout(() => {
        setCurrentCar({ visible: true, data: cars.data[index] });
        clearTimeout(temp);
      }, 500);
    }
  };

  return {
    cars: cars.data,
    currentCar,
    updateCurrentCar,
    isLoading: state.isLoading,
    error: state.error,
  };
}
