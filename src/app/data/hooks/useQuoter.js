import API_ROUTES from "app/core/const/api.routes";
import { useEffect, useState } from "react";
import carService from "../services/apis/CarService";

export default function useQuoter({ slug }) {
  const [state, setState] = useState({ isLoading: false, error: null });
  const [car, setCar] = useState(null);
  const [options, setOptions] = useState([]);

  useEffect(() => {
    
    setState({ isLoading: true, error: null });
    carService
      .getFullDetails(slug)
      .then(({ data }) => {
        const newData = {
          id: data.id,
          name: data.name,
          code: data.code,
          slug: data.slug,
          price: data.price,
          discount: data.discount,
          passenger: data.passenger,
          transmission: data.transmission,
          hp: data.hp,
          speed: data.speed,
          image: API_ROUTES.PRODUCTS.IMAGE + data.image,
        };
        setCar(newData);
        //
        data.options.forEach((option) => {
          option.items.forEach((item) => {
            item.image = API_ROUTES.PRODUCTS.IMAGE + item.image;
            item.isSelected = false;
          });
        });
        //
        setOptions(data.options);
        //
        setState({ isLoading: false, error: null });
      })
      .catch((errorRes) => {
        setState({ isLoading: false, error: errorRes.message });
      });
  }, [slug]);

  return {
    car,
    options,
    isLoading: state.isLoading,
    error: state.error,
  };
}
