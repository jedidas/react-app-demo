import React, { useState } from "react";

const initialData = {
  data: [],
  from: 0,
  last_page: 0,
  total: 0,
};
export const CarContext = React.createContext(initialData);

export default function CarContextProvider({ children }) {
  const [cars, setCar] = useState(initialData);

  const findBySlug = (slug) => cars.data.find((car) => car.slug === slug) || null;
  const findById = (id) => cars.data.find((car) => car.id === id) || null;

  return (
    <CarContext.Provider value={{ cars, setCar, findBySlug, findById }}>
      {children}
    </CarContext.Provider>
  );
}
