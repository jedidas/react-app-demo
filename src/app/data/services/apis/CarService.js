import API_ROUTES from "app/core/const/api.routes";

const { default: ApiService } = require("./ApiService");

class CarService extends ApiService {
  getAll() {
    return this.get(`${API_ROUTES.PRODUCTS.DEFAULT}`);
  }
  getDetails = (slug) => {
    return this.get(`${API_ROUTES.PRODUCTS.DETAIL_SLUG.replace("REPACE_ME", slug)}`);
  };

  getFullDetails = (slug) => {
    return this.get(`${API_ROUTES.PRODUCTS.DETAIL_FULL_SLUG.replace("REPACE_ME", slug)}`);
  };
}
const carService = new CarService();
export default carService;
