import React from "react";
import "./error-message.scss";

const ErrorMessageComponent = ({ code = 404, message }) => {
  return (
    <div className="app__error-message">
      <h2>{code}</h2>
      <p>{message}</p>
    </div>
  );
};

export default React.memo(ErrorMessageComponent);
