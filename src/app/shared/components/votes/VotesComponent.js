import { numberFormat } from "app/core/utils/utils";
import React from "react";
import "./votes-component.scss";

const VotesComponent = ({ dislikes = 0, likes = 0, vote = null }) => {
  const getVote = (className = "", type = "") => {
    if (type === "like" && vote === true) {
      return `${className} active`;
    }
    if (type === "dislike" && vote === false) {
      return `${className} active`;
    }
    return className;
  };

  return (
    <div className="app__votes-component">
      <button
        type="button"
        className={getVote("app__votes-component_btn", "like")}
      >
        {numberFormat(likes)} <i className="icon-likes"></i>
      </button>
      <button
        type="button"
        className={getVote("app__votes-component_btn", "dislike")}
      >
        {numberFormat(dislikes)} <i className="icon-dislikes"></i>
      </button>
    </div>
  );
};

export default React.memo(VotesComponent);
