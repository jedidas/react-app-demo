import React from "react";
import "./options-component.scss";

const OptionsComponent = ({ children, name, required }) => {
  return (
    <div className="app__options">
      <h3 className="app__options_title">
        {required && <span className="required">*</span>}
        {name}
      </h3>
      <ul className="app__options_items clearfix">{children}</ul>
    </div>
  );
};

export default React.memo(OptionsComponent, (prevProps, nextProps) => {
  return prevProps.children === nextProps.children;
});
