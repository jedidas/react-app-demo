import React from "react";

const OptionItemComponent = ({ image, name, price, onClick, isSelected }) => {

  const isSelectedClass = isSelected ? "app__options_items_one-btn active" : "app__options_items_one-btn";

  return (
    <li className="app__options_items_one">
      <button className={isSelectedClass} onClick={onClick}>
        <div className="app__options_items_one_img">
          <img src={image} alt={name} />
        </div>
        <h5 className="app__options_items_one_title">{name}</h5>
      </button>
      <p className="app__options_items_one_price">${price}</p>
    </li>
  );
};

export default React.memo(OptionItemComponent);
