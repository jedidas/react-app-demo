import React from "react";

import './simple-spinner-component.scss';

const SimpleSpinnerComponent = () => {
  return <div className="app__simple-spinner-component">
      <div className="loader">Loading...</div>
  </div>;
};

export default React.memo(SimpleSpinnerComponent);
