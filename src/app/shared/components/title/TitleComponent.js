import React from "react";

import "./title-component.scss";

const TitleComponent = ({
  className = "text-center",
  leyend = "",
  subTitle = "",
  title = "",
}) => {
  const cssClass = `app__title-component ${className}`;

  return (
    <hgroup className={cssClass}>
      <h1>
        <strong>{title}</strong> {subTitle}
      </h1>
      <p className="leyend">{leyend}</p>
    </hgroup>
  );
};

export default React.memo(TitleComponent);
