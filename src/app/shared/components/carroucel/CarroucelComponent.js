import Slider from "react-slick";

import "./carroucel-component.scss";

const CarroucelComponent = ({
  beforeChange,
  children,
  settings = {
    slidesToScroll: 1,
    className: "center",
    centerMode: true,
    infinite: true,
    centerPadding: "60px",
    slidesToShow: 3,
    speed: 500,
    dots: false,
    arrows: false,
    beforeChange: (oldIndex, newIndex) => {
      beforeChange(newIndex);
    },
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          centerPadding: "40px",
        },
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          centerPadding: "40px",
        },
      },
    ],
  },
  slideRef,
}) => {
  return (
    <div className="carroucel_component">
      <Slider {...settings} ref={slideRef}>
        {children}
      </Slider>
    </div>
  );
};

export default CarroucelComponent;
