import { useFormik } from "formik";

import useAuth from "app/data/hooks/useAuth";
import SimpleSpinnerComponent from "app/shared/components/spinners/SimpleSpinnerComponent/SimpleSpinnerComponent";

export default function LoginComponent({ close }) {
  const { login, isLoading, error } = useAuth();

  const validate = (values) => {
    const errors = {};
    if (!values.password) {
      errors.password = "Required";
    } else if (values.password.length > 15) {
      errors.password = "Must be 15 characters or less";
    }

    if (!values.email) {
      errors.email = "Required";
    } else if (
      !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
      errors.email = "Invalid email address";
    }

    return errors;
  };

  const formik = useFormik({
    initialValues: {
      email: "",
      password: "",
    },
    validate,
    onSubmit: (values) => {
      login({
        email: values.email,
        password: values.password,
      })
        .then(() => {
          close();
        })
        .catch((myError) => {
          console.log("login error", myError);
        });
    },
  });

  return (
    <div className="login_app">
      <div className="login_app__container">
        <div className="container app__form">
          <div className="col-12">
            {error && <div className="alert alert-danger">{error}</div>}
          </div>
          <form onSubmit={formik.handleSubmit}>
            <div className="col-12 mb-3">
              <label className="form-label" htmlFor="email">
                Email
              </label>
              <input
                id="email"
                name="email"
                type="email"
                onChange={formik.handleChange}
                value={formik.values.email}
                className={`form-control ${formik.errors.email ? "error" : ""}`}
                placeholder="Last Name"
              />
              {formik.errors.email ? (
                <p className="error">{formik.errors.email}</p>
              ) : null}
            </div>

            <div className="col-12 mb-3">
              <label className="form-label" htmlFor="email">
                Password
              </label>
              <input
                id="password"
                name="password"
                type="password"
                onChange={formik.handleChange}
                value={formik.values.password}
                className={`form-control ${
                  formik.errors.password ? "error" : ""
                }`}
                placeholder="Password"
              />
              {formik.errors.password ? (
                <p className="error">{formik.errors.password}</p>
              ) : null}
            </div>

            <div className="col-12">
              {isLoading ? (
                <SimpleSpinnerComponent />
              ) : (
                <button className="btn btn-more light-blue" type="submit">
                  Submit
                </button>
              )}
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
